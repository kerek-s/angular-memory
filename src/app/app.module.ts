import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { IconCardComponent } from './components/icon-card/icon-card.component';
import { HeaderComponent } from './components/header/header.component';
import { ReactiveFormsModule } from '@angular/forms';
import { BaseComponent } from './components/base/base.component';
import { SelectComponent } from './components/select/select.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
	BaseComponent,
    IconCardComponent,
    HeaderComponent,
    SelectComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
	MatButtonModule,
    MatIconModule,
	ReactiveFormsModule,
	ServiceWorkerModule.register("ngsw-worker.js", {
		enabled: environment.production,
		registrationStrategy: "registerImmediately"
	})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
