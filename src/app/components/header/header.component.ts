import { ChangeDetectionStrategy, Component, Input, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent {
	@Input() clicksNumber: number;
	@Output() cardsNumberChanged = new EventEmitter<number>();
	@Output() refresh = new EventEmitter<void>();

	cardsNumber = new FormControl();
	readonly cardsNumberOptions: number[] = [8, 16, 32, 64];

	onSelectChange(cardsNumber: number):void {
		this.cardsNumberChanged.emit(cardsNumber);
	}

	onRefresh(): void {
		this.refresh.emit();
	}
}
