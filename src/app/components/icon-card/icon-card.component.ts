import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IconCard } from 'src/app/models/icon-card.interface';

@Component({
  selector: 'app-icon-card',
  templateUrl: './icon-card.component.html',
  styleUrls: ['./icon-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class IconCardComponent {
  @Input()
  set iconCard(iconCard: IconCard) {
	if(iconCard) {
		this.card = iconCard;
	}
  }
  @Output() cardClick = new EventEmitter<IconCard>();
  card: IconCard;

  onCardClick(): void {
    this.cardClick.emit(this.card);
  }

}
