import { Component, OnDestroy, OnInit } from '@angular/core';
import { BehaviorSubject, Observable, Subject, Subscription } from 'rxjs';
import { matIconNames } from './consts/material-icon-names';
import { Icon, IconCard } from './models/icon-card.interface';
import { delay, filter, first, takeUntil, tap } from 'rxjs/operators';
import { BaseComponent } from './components/base/base.component';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
	iconNames: string[] = matIconNames;
	iconCards: Record<number, IconCard>;
	clicksNumber: number = 0;
	control: number = 0;
	cardsNumber: number;
	private pairSubject = new BehaviorSubject<Icon[]>([]);
	private hiderSubscription = new Subscription();
	private showerSubscription = new Subscription();
	private pairSubscription = new Subscription();


	get pair$(): Observable<Icon[]> {
		return this.pairSubject.asObservable();
	}

	ngOnInit(): void {
		this.subsctibeToPair();
	}

	ngOnDestroy(): void {
		this.allUnSubscrition();
	}

	private subsctibeToPair(): void {
		this.hiderSubscription = this.pair$
			.pipe(
				filter(
					(pair) => pair.length === 2 && pair[0].name !== pair[1].name
				),
				delay(2000)
			)
			.subscribe((pair) => {
				pair.forEach((item) => {
					this.iconCards[item.id].visible = false;
				});
				this.pairSubject.next([]);
			});

		this.showerSubscription = this.pair$
			.pipe(
				tap((pair) => {
					if ([1, 2].includes(pair.length)) {
						pair.forEach((item) => {
							this.iconCards[item.id].visible = true;
						});
					}
				})
			)
			.subscribe(() => {});

		this.pairSubscription = this.pair$.subscribe((pair) => {
			if (pair.length === 2 && pair[0].name === pair[1].name) {
				this.iconCards[pair[0].id].hasPair = true;
				this.iconCards[pair[1].id].hasPair = true;
				this.control += 2;
				this.pairSubject.next([]);
			}
		});
	}

	private allUnSubscrition(): void {
		this.hiderSubscription.unsubscribe();
	 	this.showerSubscription.unsubscribe();
	 	this.pairSubscription.unsubscribe();
	}

	onRefresh(): void {
		this.onCardsNumberChanged(this.cardsNumber);
	}

	onCardsNumberChanged(cardsNumber: number): void {
		this.allUnSubscrition();
		this.subsctibeToPair();
		navigator.vibrate(200);
		this.clicksNumber = 0;
		this.control = 0;
		this.pairSubject.next([]);
		this.createIconObjs(cardsNumber);
	}

	onCardClick(iconCard: IconCard): void {
		if (iconCard?.hasPair || iconCard?.visible) {
			return;
		}

		this.pair$
			.pipe(
				first(),
				filter((pair) => pair.length < 2)
			)
			.subscribe((pair) => {
				const ids = pair.reduce((acc, item) => [...acc, item.id], []);
				if (ids.indexOf(iconCard.id) === -1) {
					++this.clicksNumber;
					this.pairSubject.next([
						...pair,
						{ id: iconCard.id, name: iconCard.name },
					]);
				}
			});
	}

	private shuffle(shufflable: any[]): any[] {
		return shufflable.sort((a, b) => 0.5 - Math.random());
	}

	private createIconObjs(cardsNumber: number): void {
		this.cardsNumber = cardsNumber;
		let indexes: number[] = [];
		for (let i = 0; i < cardsNumber; ++i) {
			indexes.push(i);
		}
		this.shuffle(indexes);
		const table: IconCard[] = [];
		this.shuffle(this.iconNames)
			.slice(0, cardsNumber / 2)
			.forEach((iconName, index) => {
				table.push({
					name: iconName,
					id: indexes.shift(),
					visible: false,
					hasPair: false,
				});
				table.push({
					name: iconName,
					id: indexes.shift(),
					visible: false,
					hasPair: false,
				});
			});
		this.iconCards = table.reduce(
			(acc, icon) => ({ ...acc, [icon.id]: icon }),
			{}
		);
	}
}
