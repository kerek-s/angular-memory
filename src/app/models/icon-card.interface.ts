export interface Icon {
  id?: number;
  name: string;
}

export interface IconCard extends Icon {
  visible: boolean;
  hasPair: boolean;
}
